package punklan.zerozero.vadik.server.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Frost on 18.12.2016.
 */
@Entity
@Table(name = "location")
public class Location {
    @JsonIgnore
    @ManyToOne
    private Person person;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "state")
    private LocationState state;

    @Column(name = "timestamp")
    private Date timestamp;

    @Column(name = "latitude")
    private double latitude;

    @Column(name = "longitude")
    private double longitude;

    protected Location() {
    }

    public Location(Person person, double latitude, double longitude) {
        this.person = person;
        this.state = LocationState.NEW;
        this.timestamp = new Date();
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public long getId() {
        return id;
    }

    public Person getPerson() {
        return person;
    }

    public LocationState getState() {
        return state;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    private enum LocationState {
        NEW,
        PROCESSED
    }
}
