package punklan.zerozero.vadik.server.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Frost on 08.12.2016.
 */
@Entity
@Table(name = "person")
public class Person {
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "person")
    private Set<Location> locations = new HashSet<>();

    @OneToOne(mappedBy = "personHost")
    private Party party = null;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER, mappedBy = "members")
    private Set<Party> parties = new HashSet<>();

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id = null;

    @Column(name = "name")
    private String name;

    @Column(name = "phone")
    private String phone;

    protected Person() {
    }

    public Person(String name) {
        this.name = name;
    }

    public Person(String name, String phone) {
        this.name = name;
        this.phone = phone;
    }

    public Set<Location> getLocations() {
        return locations;
    }

    public Set<Party> getParties() {
        return parties;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }

    public Party getParty() {
        return party;
    }
}
