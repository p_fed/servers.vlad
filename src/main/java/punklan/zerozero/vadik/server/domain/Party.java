package punklan.zerozero.vadik.server.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Frost on 09.12.2016.
 */
@Entity
@Table(name = "party")
public class Party {

    @JsonIgnore
    @OneToOne
    private Person personHost;

    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "party_member",
                joinColumns = @JoinColumn(name = "party_id", referencedColumnName = "id"),
                inverseJoinColumns = @JoinColumn(name = "person_id", referencedColumnName = "id"))
    private Set<Person> members = new HashSet<>();

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    @Column(name = "name")
    private String name;

    @Column(name = "state")
    private PartyState state;

    protected Party() {
    }

    public Party(Person personHost, String name) {
        this.name = name;
        this.personHost = personHost;
        this.state = PartyState.ACTIVE;
    }

    public Person getPersonHost() {
        return personHost;
    }

    public Set<Person> getMembers() {
        return members;
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public PartyState getState() {
        return state;
    }

    private enum PartyState {
        ACTIVE,
        INACTIVE
    }
}
