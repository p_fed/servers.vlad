package punklan.zerozero.vadik.server.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import punklan.zerozero.vadik.server.domain.Party;
import punklan.zerozero.vadik.server.domain.Person;
import punklan.zerozero.vadik.server.exception.PersonNotFoundException;
import punklan.zerozero.vadik.server.repository.PartyRepository;
import punklan.zerozero.vadik.server.repository.PersonRepository;

import java.net.URI;

/**
 * Created by Frost on 21.12.2016.
 */
@RestController
@RequestMapping("/api/1.0/{personId}/parties")
public class PartyResource {
    private PersonRepository personRepository;
    private PartyRepository partyRepository;

    @Autowired
    public PartyResource(PersonRepository personRepository, PartyRepository partyRepository) {
        this.personRepository = personRepository;
        this.partyRepository = partyRepository;
    }

    @RequestMapping(method = RequestMethod.POST)
    ResponseEntity<?> add(@PathVariable Long personId, @RequestBody Party party) {
        this.validatePerson(personId);

        Person person = this.personRepository.findById(personId);

        Party result = this.partyRepository.save(new Party(person, party.getName()));

        URI uri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(result.getId())
                .toUri();
        return ResponseEntity.created(uri).build();
    }

    @RequestMapping(method = RequestMethod.GET, value = "{partyId}")
    Party getParty(@PathVariable Long personId, @PathVariable Long partyId){
        this.validatePerson(personId);

        return this.partyRepository.findById(partyId);
    }

    private void validatePerson(Long personId) {
        if (this.personRepository.findById(personId) == null) {
            throw new PersonNotFoundException(personId);
        }
    }
}
