package punklan.zerozero.vadik.server.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import punklan.zerozero.vadik.server.domain.Location;
import punklan.zerozero.vadik.server.domain.Person;
import punklan.zerozero.vadik.server.exception.PersonNotFoundException;
import punklan.zerozero.vadik.server.repository.LocationRepository;
import punklan.zerozero.vadik.server.repository.PersonRepository;

import java.net.URI;

/**
 * Created by Frost on 21.12.2016.
 */
@RestController
@RequestMapping("/api/1.0/{personId}/locations")
public class LocationResource {
    private PersonRepository personRepository;
    private LocationRepository locationRepository;


    @Autowired
    public LocationResource(PersonRepository personRepository, LocationRepository locationRepository) {
        this.personRepository = personRepository;
        this.locationRepository = locationRepository;
    }

    @RequestMapping(method = RequestMethod.POST)
    ResponseEntity<?> add(@PathVariable Long personId, @RequestBody Location location) {
        validatePerson(personId);

        Person person = this.personRepository.findById(personId);

        Location result = this.locationRepository.save(new Location(person, location.getLatitude(), location.getLongitude()));
        URI uri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(result.getId())
                .toUri();
        return ResponseEntity.created(uri).build();
    }

    @RequestMapping(method = RequestMethod.GET, value = "{locationId}")
    Location getLocation(@PathVariable Long personId, @PathVariable Long locationId) {
        validatePerson(personId);

        return this.locationRepository.findById(locationId);
    }

    private void validatePerson(Long personId) {
        if (this.personRepository.findById(personId) == null) {
            throw new PersonNotFoundException(personId);
        }
    }
}
