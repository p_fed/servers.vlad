package punklan.zerozero.vadik.server.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import punklan.zerozero.vadik.server.domain.Party;
import punklan.zerozero.vadik.server.domain.Person;
import punklan.zerozero.vadik.server.repository.PartyRepository;
import punklan.zerozero.vadik.server.repository.PersonRepository;

/**
 * Created by Frost on 22.12.2016.
 */
@RestController
@RequestMapping("/api/1.0/parties/{partyId}/members")
public class MemberResource
{
    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private PartyRepository partyRepository;

    @RequestMapping(method = RequestMethod.POST)
    Person addMember(@PathVariable Long partyId, @RequestBody Person member)
    {

        Party party = partyRepository.findById(partyId);

        Person person = personRepository.save(new Person(member.getName(), member.getPhone()));
        party.getMembers().add(person);
        partyRepository.save(party);


        return person;
    }
}
