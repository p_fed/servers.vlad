package punklan.zerozero.vadik.server.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import punklan.zerozero.vadik.server.domain.Person;
import punklan.zerozero.vadik.server.repository.PersonRepository;

import java.net.URI;

/**
 * Created by Frost on 10.12.2016.
 */
@RestController
@RequestMapping("/api/1.0/person")
public class PersonResource {
    @Autowired
    PersonRepository personRepository;

    @RequestMapping(method = {RequestMethod.POST}, produces = {MediaType.APPLICATION_JSON_VALUE})
    ResponseEntity<?> add(@RequestBody Person person) {
        Person result = createPerson(person.getName(), person.getPhone());
        URI uri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(result.getId())
                .toUri();
        return ResponseEntity.created(uri).build();
    }

    public Person createPerson(String name, String phone){
        return personRepository.save( new Person(name, phone));
    }


    @RequestMapping(value = "/{personId}", method = {RequestMethod.GET}, produces = {MediaType.APPLICATION_JSON_VALUE})
    Person getOne(@PathVariable(value = "personId") Long personId) {
        return this.personRepository.findById(personId);
    }
}
