package punklan.zerozero.vadik.server.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import punklan.zerozero.vadik.server.domain.Location;

import java.util.Collection;

/**
 * Created by Frost on 20.12.2016.
 */
public interface LocationRepository extends JpaRepository<Location, Long> {
    Collection<Location> findByPersonId(Long id);

    Location findById(Long id);
}
