package punklan.zerozero.vadik.server.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import punklan.zerozero.vadik.server.domain.Party;

import javax.transaction.Transactional;

/**
 * Created by Frost on 12.12.2016.
 */
@Repository
@Transactional
public interface PartyRepository extends JpaRepository<Party, Long> {

    Party findById(Long id);

}
