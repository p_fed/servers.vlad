package punklan.zerozero.vadik.server.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import punklan.zerozero.vadik.server.domain.Person;

import java.util.List;

/**
 * Created by Frost on 09.12.2016.
 */
//@Repository
//@Transactional
public interface PersonRepository extends JpaRepository<Person, Long> {
    List<Person> findByName(String name);

    Person findById(Long id);
}
