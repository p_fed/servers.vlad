package punklan.zerozero.vadik.server.exception;

/**
 * Created by Frost on 21.12.2016.
 */
public class PersonNotFoundException extends RuntimeException {
    public PersonNotFoundException(Long persoId) {
        super("could not find person '" + persoId + "'.");
    }
}
